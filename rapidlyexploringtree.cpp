#include "rapidlyexploringtree.h"
#include <iostream>

constexpr int max_x = 138;
constexpr int min_x = 1;
constexpr int max_y = 118;
constexpr int min_y = 1;
constexpr int max_iterations = 10000;
constexpr double delta_q = 1.0;         //max step between nodes
constexpr double goal_tolerance = 2.0;

RapidlyExploringTree::RapidlyExploringTree(std::shared_ptr<Map> map)
    : _map(map)
{
    std::cout << "Rapidly Exploring Tree instance created!" << std::endl;
}

RapidlyExploringTree::~RapidlyExploringTree()
{
    reset();
    std::cout << "Rapidly Exploring Tree instance deleted!" << std::endl;
}

Node RapidlyExploringTree::randomConfig()
{
    double x = static_cast<double>(arc4random() / arc4random() * (max_x - min_x) + min_x);
    double y = static_cast<double>(arc4random() / arc4random() * (max_y - min_y) + min_y);
    return Node(QPoint(x, y));
}

Node* RapidlyExploringTree::nearest(Tree& tree, Node q)
{
    Node* nearest_node = nullptr;
    double min_dist = std::numeric_limits<double>::max();
    for (auto& node : tree.nodes)
    {
        double dist = node.distance(q);
        if (dist < min_dist)
        {
            min_dist = dist;
            nearest_node = &node;
        }
    }
    return nearest_node;
}

Node RapidlyExploringTree::newConfig(Node q, Node q_near, double delta_q)
{
    double dist = q_near.distance(q);
    double step_size = arc4random() / double(RAND_MAX) * delta_q; // Randomly choose a step size between 0 and delta_q
    if (dist <= step_size) // If the distance to q_near is less than the step size, return q
    {
        return q;
    }
    double ratio = step_size / dist;
    double dx = q.point.x() - q_near.point.x();
    double dy = q.point.y() - q_near.point.y();
    double x = q_near.point.x() + dx * ratio; //+ arc4random() / double(RAND_MAX) * delta_q;// - delta_q / 2.0; // Add some randomness to the x-coordinate of the new configuration
    double y = q_near.point.y() + dy * ratio; //+ arc4random() / double(RAND_MAX) * delta_q ;//- delta_q / 2.0; // Add some randomness to the y-coordinate of the new configuration
    return Node(QPoint(x, y));
}



bool RapidlyExploringTree::isColisionFree(const Node& node1, const Node& node2)
{
    // Check if the nodes are adjacent or diagonal to each other
    bool isAdjacent = abs(node1.point.x() - node2.point.x()) <= 1 && abs(node1.point.y() - node2.point.y()) <= 1;

    if (isAdjacent) {
        for (const auto& obstacle : _map->getObstacles())
        {
            if (node2.point == obstacle)
                return false;
        }
        return true;
    }

    // Calculate the cells between the two nodes
    std::vector<Node> cells = getCellsOnLine(node1, node2);

    // Check if any of the cells are obstacles
    for (const auto& cell : cells) {
        for (const auto& obstacle : _map->getObstacles())
        {
            if (cell.point == obstacle)
                return false;
        }
    }

    return true;
}


std::vector<Node> RapidlyExploringTree::getCellsOnLine(const Node& node1, const Node& node2)
{
    std::vector<Node> cellsOnLine;

    int x1 = node1.point.x();
    int y1 = node1.point.y();
    int x2 = node2.point.x();
    int y2 = node2.point.y();

    int dx = abs(x2 - x1);
    int dy = abs(y2 - y1);
    int sx = (x1 < x2) ? 1 : -1;
    int sy = (y1 < y2) ? 1 : -1;
    int err = dx - dy;

    while (true) {
        cellsOnLine.push_back(Node(QPoint(x1, y1)));
        if (x1 == x2 && y1 == y2) break;
        int e2 = 2 * err;
        if (e2 > -dy) {
            err -= dy;
            x1 += sx;
        }
        if (e2 < dx) {
            err += dx;
            y1 += sy;
        }
    }

    return cellsOnLine;
}


// function to generate a new configuration that
// moves towards a given configuration q from a given
// configuration q_near, with a maximum step size delta_q
Node newConfig(Node q, Node q_near, double delta_q)
{
    double dist = q_near.distance(q);
    if (dist < delta_q)
    {
        return q;
    }
    double x = q_near.point.x() + delta_q / dist * (q.point.x() - q_near.point.x());
    double y = q_near.point.y() + delta_q / dist * (q.point.y() - q_near.point.y());
    return Node(QPoint(x, y));
}

void RapidlyExploringTree::run()
{
    Tree tree(_map->getStartPoint());
    for (int i = 0; i < max_iterations; i++)
    {
        Node q_rand = randomConfig();
        Node* q_near = nearest(tree, q_rand);
        Node q_new = newConfig(q_rand, *q_near, delta_q);
        _visitedCells.push_back(q_new.point);

        if(i % 10)
            emit visitedCellsUpdated(_visitedCells);

        if (isColisionFree(*q_near, q_new))
        {
            tree.addNode(q_new);
            tree.addEdge(*q_near, q_new);
            _path.push_back(q_new.point);
            if (q_new.point == _map->getTargetPoint() || q_new.distance(Node(_map->getTargetPoint())) < goal_tolerance)
            {
                std::cout << "Goal reached!" << std::endl;
                emit isCalculating(false);
                emit pathFound(_path);
                return;
            }
        }
    }

    emit isCalculating(false);
    emit noPathFound();
    emit pathFound(_path);
    std::cout << "Goal not reached." << std::endl;
}

void RapidlyExploringTree::reset()
{
    _visitedCells.clear();
    _path.clear();
}
