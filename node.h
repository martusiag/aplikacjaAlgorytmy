#ifndef NODE_H
#define NODE_H

#include <memory>
#include <QPoint>
#include <vector>

struct Node
{
    QPoint point;

    double gCost; //cost from start
    double hCost; //heuristic cost to end
    double fCost; //total cost
    double rhsCost; //dstar gCost

    std::shared_ptr<Node> parent;

    std::vector<std::shared_ptr<Node>> successors;

    explicit Node(const QPoint& point) : point(point), gCost(0.0), hCost(0.0), fCost(0.0), rhsCost(0.0), parent(nullptr) {}

    Node(const Node& node) : point(node.point), gCost(node.gCost), hCost(node.hCost), fCost(node.fCost), rhsCost(node.rhsCost), parent(node.parent) {}

    Node& operator=(const Node& node)
    {
        if( &node != this )
        {
            this->point = node.point;
            this->fCost = node.fCost;
            this->gCost = node.gCost;
            this->hCost = node.hCost;
            this->rhsCost = node.rhsCost;
            this->parent = node.parent;
        }
        return *this;
    }

    double distance(const Node& node)
    {
        double dx = this->point.x() - node.point.x();
        double dy = this->point.y() - node.point.y();

        return std::sqrt(pow(dx,2) + pow(dy,2));
    }
};

#endif // NODE_H
