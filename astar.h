#ifndef ASTAR_H
#define ASTAR_H

#include "map.h"
#include "node.h"
#include "ialgorithm.h"

#include <QTimer>

class AStar : public IAlgorithm
{
public:

    AStar(std::shared_ptr<Map> map);

    virtual ~AStar() override;

    void run() override;

    void reset() override;

private:

    std::shared_ptr<Map> _map;

    std::vector<std::shared_ptr<Node>> _openList;
    std::vector<std::shared_ptr<Node>> _closedList;

    std::vector<QPoint> _visitedCells;
    std::vector<QPoint> _path;

    std::vector<std::shared_ptr<Node>> getNeighbours(const QPoint& cell);

    void removeNode(std::vector<std::shared_ptr<Node>>& list, std::shared_ptr<Node> node);
    bool isInList(std::vector<std::shared_ptr<Node>>& list, std::shared_ptr<Node> node);

    double calculateHeuristic(const QPoint& point1, const QPoint& point2);
};

#endif // ASTAR_H
