#include "bug.h"
#include <iostream>

constexpr int maxIterations = 100000;

Bug::Bug(std::shared_ptr<Map> map)
    : _map(map)

{
    std::cout << "BUG instance created!" << std::endl;
}

Bug::~Bug()
{
    reset();
    std::cout << "BUG instance deleted!" << std::endl;
}

void Bug::run()
{
    QPoint currentCell = _map->getStartPoint();
    bool onBoundary = false;
    int iteration = 0;


    while(currentCell != _map->getTargetPoint())
    {
        if(iteration++ == maxIterations)
        {
            return;
            emit noPathFound();
            emit pathFound(_path);
        }

        std::vector<QPoint> neigbours = getNeigbours(currentCell);
        bool obstacleContact = std::any_of(neigbours.begin(), neigbours.end(),
                                           [&](const QPoint& neighbour){ return _map->at(neighbour) == NodeState::Obstacle;});

        std::vector<QPoint> obstacleBoundary;

        if(!obstacleContact)
        {
            currentCell = getNextCell(currentCell, neigbours);
            onBoundary = false;
        }
        else if (!onBoundary)
        {
            // Determine the obstacle boundary and move along it
            obstacleBoundary.clear();
            obstacleBoundary = getObstacleBoundary(currentCell, neigbours);
            currentCell = getNextCellForObstacleContact(currentCell, obstacleBoundary);
            onBoundary = true;
        }
        else
        {
            // Already on obstacle boundary, move along it
            currentCell = getNextCellForObstacleContact(currentCell, obstacleBoundary);
            onBoundary = false;
        }
        _path.push_back(currentCell);
    }

    emit isCalculating(false);
    emit pathFound(_path);
}

void Bug::reset()
{
    _visitedCells.clear();
    _path.clear();
}

std::vector<QPoint> Bug::getNeigbours(const QPoint &cell)
{
    std::vector<QPoint> neighbors;

    // Check each neighbor position and add to the list if it's within the map bounds
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            if (dx == 0 && dy == 0)
                continue; // skip the current cell

            const QPoint neighbor = cell + QPoint(dx, dy);
            if (neighbor.x() < 0 || neighbor.x() >= _map->getSize().width() ||
                    neighbor.y() < 0 || neighbor.y() >= _map->getSize().height())
                continue; // Skip cells outside the map
            neighbors.push_back(neighbor);
        }
    }

    return neighbors;
}

double Bug::calculateDistance(const QPoint &point1, const QPoint &point2)
{
    double dx = point1.x() - point2.x();
    double dy = point1.y() - point2.y();

    return std::sqrt(pow(dx,2) + pow(dy,2));
}

QPoint Bug::getNextCellForObstacleContact(const QPoint &currentCell, const std::vector<QPoint>& obstacleBoundary)
{
    // Determine which boundary points are visible from the current cell
    std::vector<QPoint> visiblePoints;
    for (const QPoint& boundaryPoint : obstacleBoundary) {
        bool isVisible = true;

        // Check if any obstacles are blocking the view
        for (const QPoint& neighbour : getLine(currentCell, boundaryPoint)) {
            if (_map->at(neighbour) == NodeState::Obstacle) {
                isVisible = false;
                break;
            }
        }

        if (isVisible) {
            visiblePoints.push_back(boundaryPoint);
        }
    }

    // Check if the right cell is unoccupied and return it if it is
      QPoint rightCell(currentCell.x() + 1, currentCell.y());
      if (_map->at(rightCell) == NodeState::Free) {
          return rightCell;
      }

    // Select the point that is furthest away from the target point
    QPoint nextCell = currentCell;
    double maxDistance = -1.0;
    for (const QPoint& visiblePoint : visiblePoints) {
        double distance = calculateDistance(visiblePoint, _map->getTargetPoint());
        if (distance > maxDistance) {
            maxDistance = distance;
            nextCell = visiblePoint;
            _visitedCells.push_back(nextCell);
            emit visitedCellsUpdated(_visitedCells);
        }
    }

    return nextCell;
}

std::vector<QPoint> Bug::getLine(const QPoint &point1, const QPoint &point2)
{
    std::vector<QPoint> linePoints;

    int dx = std::abs(point2.x() - point1.x());
    int dy = std::abs(point2.y() - point1.y());

    int x = point1.x();
    int y = point1.y();

    int xStep = point1.x() < point2.x() ? 1 : -1;
    int yStep = point1.y() < point2.y() ? 1 : -1;

    int error = dx - dy;
    while (x != point2.x() || y != point2.y()) {
        linePoints.push_back(QPoint(x, y));
        int error2 = error * 2;
        if (error2 > -dy) {
            error -= dy;
            x += xStep;
        }
        if (error2 < dx) {
            error += dx;
            y += yStep;
        }
    }

    linePoints.push_back(point2);

    return linePoints;
}


QPoint Bug::getNextCell(const QPoint &currentCell, const std::vector<QPoint> &neighbours)
{
    QPoint nextCell = currentCell;

    double distanceToTarget = calculateDistance(currentCell, _map->getTargetPoint());
    double minDistanceToTarget = distanceToTarget;

    for (const QPoint& neighbor : neighbours)
    {
        if (_map->at(neighbor) == NodeState::Obstacle)
        {
            continue;  // skip obstacles
        }

        double distanceToTargetViaNeighbor = calculateDistance(neighbor, _map->getTargetPoint());

        if (distanceToTargetViaNeighbor < minDistanceToTarget)
        {
            minDistanceToTarget = distanceToTargetViaNeighbor;
            nextCell = neighbor;
            _visitedCells.push_back(nextCell);
            emit visitedCellsUpdated(_visitedCells);
        }
    }

    return nextCell;
}

std::vector<QPoint> Bug::getObstacleBoundary(const QPoint &currentPoint, const std::vector<QPoint>& neighbours)
{
    std::vector<QPoint> boundary;
    QPoint currentBoundaryPoint = currentPoint;
    bool obstacleFound = false;

    while(currentBoundaryPoint != _map->getTargetPoint())
    {
        for(const QPoint& neighbour : neighbours)
        {
            if (!boundary.empty() && neighbour == boundary.back())
            {
                // We have reached the same point as before, exit the loop
                return boundary;
            }

            if(_map->at(neighbour) == NodeState::Obstacle)
            {
                if(!obstacleFound)
                {
                    obstacleFound = true;
                    boundary.push_back(neighbour);
                }
                currentBoundaryPoint = neighbour;
                break;
            }
        }
        if(!obstacleFound)
            return std::vector<QPoint>();
    }
    return boundary;
}
