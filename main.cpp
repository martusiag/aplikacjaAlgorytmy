#include "mainwindow.h"
#include "map.h"
#include "renderer.h"

#include <QApplication>
#include <QObject>
#include <QMetaObject>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::shared_ptr<Map> map =std::make_shared<Map>(QSize(141,121), QPoint(10,12), QPoint(80,76));

    std::shared_ptr<Renderer> renderer = std::make_shared<Renderer>(map);

//    QObject::connect(&potentialField, &PotentialFiled::visitedCellsUpdated, renderer.get(), &Renderer::updateVisitedCell);
//    QObject::connect(&bug, &Bug::visitedCellsUpdated, renderer.get(), &Renderer::updateVisitedCell);
//    QObject::connect(&dijkstra, &Dijkstra::visitedCellsUpdated, renderer.get(), &Renderer::updateVisitedCell);
//    QObject::connect(&astar, &AStar::visitedCellsUpdated, renderer.get(), &Renderer::updateVisitedCell);
//    QObject::connect(&rapidlyExploringTree, &RapidlyExploringTree::visitedCellsUpdated, renderer.get(), &Renderer::updateVisitedCell);

    MainWindow w(renderer, map);
    w.setUpUI();
    w.show();

    return a.exec();
}

