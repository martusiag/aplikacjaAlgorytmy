#ifndef BUG_H
#define BUG_H

#include "map.h"
#include "ialgorithm.h"

#include <QObject>

// ****     BUG 1 ALGORITHM     ***
// **** turn right on obstacle  ***

class Bug : public IAlgorithm
{

public:
    Bug(std::shared_ptr<Map> map);

    virtual ~Bug() override;

    void run() override;

    void reset() override;

private:
    std::shared_ptr<Map> _map;

    std::vector<QPoint> _visitedCells;
    std::vector<QPoint> _path;

    std::vector<QPoint> getNeigbours(const QPoint& cell);
    double calculateDistance(const QPoint& point1, const QPoint& point2);
    QPoint getNextCellForObstacleContact(const QPoint& currentCell, const std::vector<QPoint>& neighbors);
    QPoint getNextCell(const QPoint& currentCell, const std::vector<QPoint>& neighbours);
    std::vector<QPoint> getObstacleBoundary(const QPoint& currentPoint, const std::vector<QPoint>& neighbours);
    std::vector<QPoint> getLine(const QPoint &point1, const QPoint &point2);

};

#endif // BUG_H
