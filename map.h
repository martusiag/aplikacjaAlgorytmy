#ifndef MAP_H
#define MAP_H

#include <QObject>
#include <QSize>
#include <QPoint>

#include "obstacles.h"

enum NodeState {Free = 0, Obstacle, PotentialPath, Path, StartPoint, TargetPoint };

struct QPointLessThan
{
    bool operator()(const QPoint& p1, const QPoint& p2) const
    {
        if (p1.x() < p2.x())
            return true;
        else if (p1.x() > p2.x())
            return false;
        else
            return p1.y() < p2.y();
    }
};

class Map : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QPoint startPoint READ getStartPoint WRITE setStartPoint NOTIFY startPointChanged)
    Q_PROPERTY(QPoint targetPoint READ getTargetPoint WRITE setTargetPoint NOTIFY targetPointChanged)
    Q_PROPERTY(bool frameObstaclesEnabled READ getFrameObstaclesEnabled WRITE setFrameObstaclesEnabled)
    Q_PROPERTY(bool firstObstacleEnabled READ getFirstObstacleEnabled WRITE setFirstObstacleEnabled)
    Q_PROPERTY(bool secondObstacleEnabled READ getSecondObstacleEnabled WRITE setSecondObstacleEnabled)
    Q_PROPERTY(bool thirdObstacleEnabled READ getThirdObstacleEnabled WRITE setThirdObstacleEnabled)
    Q_PROPERTY(bool fourthObstacleEnabled READ getFourthObstacleEnabled WRITE setFourthObstacleEnabled)
    Q_PROPERTY(bool fifthObstacleEnabled READ getFifthObstacleEnabled WRITE setFifthObstacleEnabled)

public:
    Map(const QSize& size, const QPoint& startPoint, const QPoint& targetPoint);
    Map(const Map& map);

    QPoint getStartPoint() const;
    void setStartPoint(const QPoint& newStartPoint);

    QPoint getTargetPoint() const;
    void setTargetPoint(const QPoint& newTargetPoint);

    QSize getSize() const;

    int at(const QPoint& point) const;

    const std::vector<QPoint> &getObstacles() const;

    bool getFrameObstaclesEnabled() const;
    void setFrameObstaclesEnabled(const bool& newFrameObstaclesEnabled);

    bool getFirstObstacleEnabled() const;
    void setFirstObstacleEnabled(bool newFirstObstacleEnabled);

    bool getSecondObstacleEnabled() const;
    void setSecondObstacleEnabled(bool newSecondObstacleEnabled);

    bool getThirdObstacleEnabled() const;
    void setThirdObstacleEnabled(bool newThirdObstacleEnabled);

    bool getFourthObstacleEnabled() const;
    void setFourthObstacleEnabled(bool newFourthObstacleEnabled);

    bool getFifthObstacleEnabled() const;
    void setFifthObstacleEnabled(bool newFifthObstacleEnabled);

    bool getIsObstacle(const QPoint& point) const;

signals:
    void startPointChanged();
    void targetPointChanged();
    void obstaclesChanged();

private:
    QSize _size;

    QPoint _startPoint;
    QPoint _targetPoint;

    std::map<QPoint, int, QPointLessThan> _map;
    std::vector<QPoint> _pointsVector;
    std::vector<QPoint> _obstaclePoints;

    std::unique_ptr<Obstacles> _obstaclesManager;

    void fillObstacles(const std::vector<QPoint>& obstacles);
    void initializeObstacles();
    void mapStartPoint();
    void mapTargetPoint();

    void updateObstacles();

    bool _frameObstaclesEnabled;
    bool _firstObstacleEnabled;
    bool _secondObstacleEnabled;
    bool _thirdObstacleEnabled;
    bool _fourthObstacleEnabled;
    bool _fifthObstacleEnabled;
};

#endif // MAP_H
