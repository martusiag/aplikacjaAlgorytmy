#ifndef DSTAR_H
#define DSTAR_H

#include "node.h"
#include "map.h"
#include "ialgorithm.h"

class DStar : public IAlgorithm
{

public:

    DStar(std::shared_ptr<Map> map);

    virtual ~DStar() override;

    void run() override;

    void reset() override;

private:

    std::shared_ptr<Map> _map;

    std::vector<std::shared_ptr<Node>> _openList;

    std::vector<std::shared_ptr<Node>> _closedList;

    double calculateHeuristic(const QPoint& point1, const QPoint& point2);
    std::vector<std::shared_ptr<Node>> getCellNeighbours(const QPoint &cell);

    void removeNode(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node);
    bool isInList(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node);
    void addNode(std::vector<std::shared_ptr<Node>> &closedList, std::shared_ptr<Node> &node);
    void updateNode(std::vector<std::shared_ptr<Node>> &openList, const std::shared_ptr<Node>& node);
};

#endif // DSTAR_H
