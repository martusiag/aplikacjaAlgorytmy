#ifndef ALGORITHMSHANDLER_H
#define ALGORITHMSHANDLER_H

#include <QObject>

#include "potentialfiled.h"
#include "bug.h"
#include "dijkstra.h"
#include "dstar.h"
#include "astar.h"
#include "rapidlyexploringtree.h"
#include "ialgorithm.h"

class AlgorithmsHandler : public QObject
{
    Q_OBJECT
public:
    AlgorithmsHandler(std::shared_ptr<Map> map);

    ~AlgorithmsHandler();

    IAlgorithm* createInstance(const QString& algorithm);

private:

    std::shared_ptr<Map> _map;

};

#endif // ALGORITHMSHANDLER_H
