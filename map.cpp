#include "map.h"
#include <iostream>

Map::Map(const QSize& size, const QPoint& startPoint, const QPoint& targetPoint)
    : _size(size)
    , _startPoint(startPoint)
    , _targetPoint(targetPoint)
    , _map(std::map<QPoint, int, QPointLessThan>())
    , _pointsVector(std::vector<QPoint>(_size.width() * _size.height(), QPoint()))
    , _obstaclesManager(new Obstacles(_size))
    , _frameObstaclesEnabled(false)
    , _firstObstacleEnabled(false)
    , _secondObstacleEnabled(false)
    , _thirdObstacleEnabled(false)
    , _fourthObstacleEnabled(false)
    , _fifthObstacleEnabled(false)
{
    //Fill vector with nodes
    for (int x = 0; x < _size.width(); ++x) {
        for (int y = 0; y < _size.height(); ++y) {
            QPoint point(x, y);
            _pointsVector[y * _size.width() + x] = point;
        }
    }
    // Fill map with nodes and empty spaces
    for(const auto& point : _pointsVector)
        _map[point] = 0;

    initializeObstacles();
    fillObstacles(_obstaclePoints);

    mapStartPoint();
    mapTargetPoint();

    updateObstacles();
    initializeObstacles();
}

Map::Map(const Map &map)
{
    this->_startPoint = map._startPoint;
    this->_targetPoint = map._targetPoint;
    this->_map = map._map;
    this->_obstaclePoints = map._obstaclePoints;
    this->_pointsVector = map._pointsVector;
    this->_size = map._size;
}

QPoint Map::getStartPoint() const
{
    return _startPoint;
}

void Map::setStartPoint(const QPoint& newStartPoint)
{
    if (_startPoint == newStartPoint)
        return;

    if(newStartPoint.x() != -1)
        if(newStartPoint.y() != -1)
            _startPoint = newStartPoint;
        else
            _startPoint.setX(newStartPoint.x());
    else
        if(newStartPoint.y() != -1)
            _startPoint.setY(newStartPoint.y());

    mapStartPoint();
    emit startPointChanged();
}

QPoint Map::getTargetPoint() const
{
    return _targetPoint;
}

void Map::setTargetPoint(const QPoint& newTargetPoint)
{
    if (_targetPoint == newTargetPoint)
        return;

    if(newTargetPoint.x() != -1)
        if(newTargetPoint.y() != -1)
            _targetPoint = newTargetPoint;
        else
            _targetPoint.setX(newTargetPoint.x());
    else
        if(newTargetPoint.y() != -1)
            _targetPoint.setY(newTargetPoint.y());

    mapTargetPoint();
    emit targetPointChanged();
}

QSize Map::getSize() const
{
    return _size;
}

int Map::at(const QPoint &point) const
{
    return _map.at(point);
}

const std::vector<QPoint> &Map::getObstacles() const
{
    return _obstaclePoints;
}

void Map::fillObstacles(const std::vector<QPoint> &obstacles)
{
    for(auto& [point, value] : _map) {
        if (value == Obstacle)
            _map.at(point) = Free;
    }

    for(const auto& obstacle : obstacles)
        _map.at(obstacle) = 1;
}

void Map::initializeObstacles()
{
    _obstaclePoints.clear();
    _obstaclePoints = _obstaclesManager->getObstacles();
    fillObstacles(_obstaclePoints);
}

void Map::mapStartPoint()
{
    for(auto& [point, value] : _map) {
        if (value == StartPoint) {
            value = Free;
            break; // Assuming there's only one StartPoint in the map
        }
    }
    _map.at(_startPoint) = StartPoint;
}

void Map::mapTargetPoint()
{
    for(auto& [point, value] : _map) {
        if (value == TargetPoint) {
            value = Free;
            break; // Assuming there's only one TargetPoint in the map
        }
    }
    _map.at(_targetPoint) = TargetPoint;
}

void Map::updateObstacles()
{
    std::bitset<6> enabledObstacles;
    enabledObstacles[0] = _frameObstaclesEnabled;
    enabledObstacles[1] = _firstObstacleEnabled;
    enabledObstacles[2] = _secondObstacleEnabled;
    enabledObstacles[3] = _thirdObstacleEnabled;
    enabledObstacles[4] = _fourthObstacleEnabled;
    enabledObstacles[5] = _fifthObstacleEnabled;
    _obstaclesManager->updateEnabledObstacles(enabledObstacles);
}

bool Map::getFrameObstaclesEnabled() const
{
    return _frameObstaclesEnabled;
}

void Map::setFrameObstaclesEnabled(const bool& newFrameObstaclesEnabled)
{
    if (_frameObstaclesEnabled == newFrameObstaclesEnabled)
        return;
    _frameObstaclesEnabled = newFrameObstaclesEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getFirstObstacleEnabled() const
{
    return _firstObstacleEnabled;
}

void Map::setFirstObstacleEnabled(bool newFirstObstacleEnabled)
{
    _firstObstacleEnabled = newFirstObstacleEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getSecondObstacleEnabled() const
{
    return _secondObstacleEnabled;
}

void Map::setSecondObstacleEnabled(bool newSecondObstacleEnabled)
{
    _secondObstacleEnabled = newSecondObstacleEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getThirdObstacleEnabled() const
{
    return _thirdObstacleEnabled;
}

void Map::setThirdObstacleEnabled(bool newThirdObstacleEnabled)
{
    _thirdObstacleEnabled = newThirdObstacleEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getFourthObstacleEnabled() const
{
    return _fourthObstacleEnabled;
}

void Map::setFourthObstacleEnabled(bool newFourthObstacleEnabled)
{
    _fourthObstacleEnabled = newFourthObstacleEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getFifthObstacleEnabled() const
{
    return _fifthObstacleEnabled;
}

void Map::setFifthObstacleEnabled(bool newFifthObstacleEnabled)
{
    _fifthObstacleEnabled = newFifthObstacleEnabled;
    updateObstacles();
    initializeObstacles();
    emit obstaclesChanged();
}

bool Map::getIsObstacle(const QPoint &point) const
{
    if(_map.at(point) == Obstacle)
        return true;
    else
        return false;
}
