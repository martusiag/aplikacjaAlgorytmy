#include "astar.h"
#include <iostream>

AStar::AStar(std::shared_ptr<Map> map)
    : _map(map)
{
    std::cout << "A* instance created!" << std::endl;
}

AStar::~AStar()
{
    std::cout << "A* instance deleted!" << std::endl;
    reset();
}

void AStar::run()
{
    std::cout << "A* run!" << std::endl;
    std::cout << "Map target: " << _map->getTargetPoint().x() << "," << _map->getTargetPoint().y() << std::endl;

    std::shared_ptr<Node> startNode = std::make_shared<Node>(_map->getStartPoint());
    startNode->gCost = 0;
    startNode->hCost = calculateHeuristic(startNode->point, _map->getTargetPoint());
    startNode->fCost = startNode->gCost + startNode->hCost;
    _openList.push_back(startNode);

    while(!_openList.empty())
    {
        // Find the node with the lowest fCost in the open list
        std::shared_ptr<Node> currentNode = _openList.front();
        for(const auto& node : _openList)
        {
            if(node->fCost < currentNode->fCost)
            {
                currentNode = node;
                _visitedCells.push_back(currentNode->point);
                emit visitedCellsUpdated(_visitedCells);
            }
        }

        std::cout << "currentNode: " << currentNode->point.x() << "," << currentNode->point.y() << std::endl;

        removeNode(_openList, currentNode);
        _closedList.push_back(currentNode);

        _path.push_back(currentNode->point);

        if(currentNode->point == _map->getTargetPoint())
        {
            std::cout << "A*: path found!" << std::endl;

            emit isCalculating(false);

            std::vector<QPoint> path;
            std::shared_ptr<Node> current = currentNode;
            while(current != nullptr)
            {
                path.push_back(current->point);
                current = current->parent;
            }
            std::reverse(path.begin(), path.end());

            emit pathFound(path);
            return;
        }

        std::vector<std::shared_ptr<Node>> neighbours = getNeighbours(currentNode->point);

        for(const auto& neighbour : neighbours)
        {
            if(isInList(_closedList, neighbour))
                continue;

            double tenativeGScore = currentNode->gCost + calculateHeuristic(currentNode->point, neighbour->point);

            if(!isInList(_openList, neighbour) || tenativeGScore < neighbour->gCost)
            {
                neighbour->gCost = tenativeGScore;
                neighbour->hCost = calculateHeuristic(neighbour->point, _map->getTargetPoint());
                neighbour->fCost = neighbour->gCost + neighbour->hCost;
                neighbour->parent = currentNode;

                if(!isInList(_openList, neighbour))
                {
                    _openList.push_back(neighbour);
                    std::cout << "added neighbour: " << neighbour->point.x() << "," << neighbour->point.y() << std::endl;
                }
            }
        }

    }

    emit isCalculating(false);
    emit noPathFound();
    std::cout << "No path found" << std::endl;
}

void AStar::reset()
{
    _openList.clear();
    _closedList.clear();
    _visitedCells.clear();
    _path.clear();
}

std::vector<std::shared_ptr<Node> > AStar::getNeighbours(const QPoint &cell)
{
    std::vector<std::shared_ptr<Node>> neighbors;

    // Check each neighbor position and add to the list if it's within the map bounds
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            if (dx == 0 && dy == 0)
                continue; // skip the current cell

            const QPoint neighbor = cell + QPoint(dx, dy);
            if (neighbor.x() < 0 || neighbor.x() >= _map->getSize().width() ||
                    neighbor.y() < 0 || neighbor.y() >= _map->getSize().height() ||
                    _map->at(neighbor) == NodeState::Obstacle)
                continue; // Skip cells outside the map or obstacles
            neighbors.push_back(std::make_shared<Node>(Node(neighbor)));
        }
    }

    return neighbors;
}


void AStar::removeNode(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node)
{
    for(const auto& entity : list)
    {
        if(entity->point == node->point)
        {
            const auto it = find(list.begin(), list.end(), entity);
            const int index = it - list.begin();
            list.erase(list.begin() + index);
            return;
        }
    }
}

bool AStar::isInList(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node)
{
    for(const auto& entity : list)
        if(entity->point == node->point)
            return true;

    return false;
}

double AStar::calculateHeuristic(const QPoint &point1, const QPoint &point2)
{
    double dx = point1.x() - point2.x();
    double dy = point1.y() - point2.y();

    return std::sqrt(pow(dx,2) + pow(dy,2));
}
