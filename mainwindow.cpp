#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

MainWindow::MainWindow(std::shared_ptr<Renderer> renderer, std::shared_ptr<Map> map)
    : QMainWindow()
    , ui(new Ui::MainWindow)
    , scene(new QGraphicsScene(this))
    , _map(map)
    , _renderer(renderer)
    , _algorithmsHandler(new AlgorithmsHandler(_map))
    , _isCalculating(false)
{
    ui->setupUi(this);

    QObject::connect(_renderer.get(), &Renderer::refreshView, this, &MainWindow::refreshViewSlot);

    QObject::connect(_map.get(), &Map::startPointChanged, _renderer.get(), &Renderer::updateImage);
    QObject::connect(_map.get(), &Map::targetPointChanged, _renderer.get(), &Renderer::updateImage);

    scene->setSceneRect(0, 0, 700, 600);
    ui->mapGraphicsView->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    ui->mapGraphicsView->setScene(scene.get());
    ui->mapGraphicsView->setSceneRect(0,0,700,600);
    ui->mapGraphicsView->setTransform(QTransform::fromTranslate(0,0));
    ui->mapGraphicsView->setBackgroundBrush(QBrush(Qt::transparent));
    ui->mapGraphicsView->setStyleSheet("background-color: transparent;");
    scene->addItem(const_cast<QGraphicsItem*>(renderer->rederer()));
}

void MainWindow::setUpUI()
{
    connect(ui->AlgorithmsComboBox, &QComboBox::currentTextChanged, this, &MainWindow::onCurrentTextChanged);
    connect(ui->InitialPointXInput, &QLineEdit::textChanged, this, &MainWindow::setStartPointX);
    connect(ui->InitialPointYInput, &QLineEdit::textChanged, this, &MainWindow::setStartPointY);

    ui->InitialPointXInput->setValidator(new QIntValidator(0,141,ui->InitialPointXInput));

    connect(ui->TargetPointXInput, &QLineEdit::textChanged, this, &MainWindow::setTargetPointX);
    connect(ui->TargetPointYInput, &QLineEdit::textChanged, this, &MainWindow::setTargetPointY);

    connect(ui->SetPathButton, &QPushButton::clicked, this, &MainWindow::startAlgorithm);
    connect(ui->ResetButton, &QPushButton::clicked, this, &MainWindow::reset);

    ui->AlgorithmNameLabel->setText(ui->AlgorithmsComboBox->currentText());

    _algorithm.reset(_algorithmsHandler->createInstance(ui->AlgorithmsComboBox->currentText()));
    connect(_algorithm.get(), &IAlgorithm::visitedCellsUpdated, _renderer.get(), &Renderer::updateVisitedCell);
    connect(_algorithm.get(), &IAlgorithm::pathFound, _renderer.get(), &Renderer::updatePath);
    connect(_algorithm.get(), &IAlgorithm::isCalculating, this, &MainWindow::changeStatus);
    connect(_algorithm.get(), &IAlgorithm::noPathFound, this, &MainWindow::onPathNotFound);

    ui->currentStatusLabel->setText("Idle");

    connect(ui->Obstacle1EnableCheckbox, &QCheckBox::stateChanged, _map.get(), &Map::setFirstObstacleEnabled);
    connect(ui->Obstacle2EnableCheckbox, &QCheckBox::stateChanged, _map.get(), &Map::setSecondObstacleEnabled);
    connect(ui->Obstacle3EnableCheckbox, &QCheckBox::stateChanged, _map.get(), &Map::setThirdObstacleEnabled);
    connect(ui->Obstacle4EnableCheckbox, &QCheckBox::stateChanged, _map.get(), &Map::setFourthObstacleEnabled);
    connect(ui->Obstacle5EnableCheckbox, &QCheckBox::stateChanged, _map.get(), &Map::setFifthObstacleEnabled);
    connect(ui->FrameObstaclesCheckBox, &QCheckBox::stateChanged, _map.get(), &Map::setFrameObstaclesEnabled);

    connect(_map.get(), &Map::obstaclesChanged, _renderer.get(), &Renderer::updateImage);
}

void MainWindow::startAlgorithm()
{
    if(ui->currentStatusLabel->text().contains("Done") || ui->currentStatusLabel->text().contains("Path not found") )
    {
        reset();
        QTime dieTime= QTime::currentTime().addSecs(1);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
    }

    if(_algorithm != nullptr)
    {
        changeStatus(true);
        QTimer::singleShot(200, _algorithm.get(), &IAlgorithm::run);
    }
}

void MainWindow::reset()
{
    ui->currentStatusLabel->setText("Idle");
    _renderer->reset();
    _algorithm->reset();
}

void MainWindow::changeStatus(const bool &isCalculating)
{
    std::cout << "Change status: " << (isCalculating ? "1" : "0") << std::endl;
    if(isCalculating)
        ui->currentStatusLabel->setText("Calculating");
    else if(!isCalculating && _isCalculating)
        ui->currentStatusLabel->setText("Done");
    else
        ui->currentStatusLabel->setText("Idle");

    _isCalculating = isCalculating;
}

void MainWindow::onPathNotFound()
{
    ui->currentStatusLabel->setText("Path not found");
}

void MainWindow::onCurrentTextChanged(const QString &text)
{
    _renderer->reset();
    ui->currentStatusLabel->setText("Idle");
    disconnect(_algorithm.get(), &IAlgorithm::visitedCellsUpdated, _renderer.get(), &Renderer::updateVisitedCell);
    disconnect(_algorithm.get(), &IAlgorithm::pathFound, _renderer.get(), &Renderer::updatePath);
    disconnect(_algorithm.get(), &IAlgorithm::isCalculating, this, &MainWindow::changeStatus);
    disconnect(_algorithm.get(), &IAlgorithm::noPathFound, this, &MainWindow::onPathNotFound);
    _algorithm.reset(_algorithmsHandler->createInstance(text));
    connect(_algorithm.get(), &IAlgorithm::visitedCellsUpdated, _renderer.get(), &Renderer::updateVisitedCell);
    connect(_algorithm.get(), &IAlgorithm::pathFound, _renderer.get(), &Renderer::updatePath);
    connect(_algorithm.get(), &IAlgorithm::isCalculating, this, &MainWindow::changeStatus);
    connect(_algorithm.get(), &IAlgorithm::noPathFound, this, &MainWindow::onPathNotFound);

    ui->AlgorithmNameLabel->setText(text);
}

void MainWindow::setStartPointX(const QString &text)
{
    if(text.isEmpty())
        return;

    if(text.toInt() < 141 && text.toInt() > -1)
        if(!_map->getIsObstacle(QPoint(text.toInt(), _map->getStartPoint().y())))
            _map->setStartPoint(QPoint(text.toInt(nullptr, 10), -1));
}

void MainWindow::setStartPointY(const QString &text)
{
    if(text.isEmpty())
        return;

    if(text.toInt() < 121 && text.toInt() > -1)
        if(!_map->getIsObstacle(QPoint(_map->getStartPoint().x(), text.toInt())))
        _map->setStartPoint(QPoint(-1, text.toInt(nullptr, 10)));
}

void MainWindow::setTargetPointX(const QString &text)
{
    if(text.isEmpty())
        return;

    if(text.toInt() < 141 && text.toInt() > -1)
        if(!_map->getIsObstacle(QPoint(text.toInt(), _map->getTargetPoint().y())))
            _map->setTargetPoint(QPoint(text.toInt(nullptr, 10), -1));
}

void MainWindow::setTargetPointY(const QString &text)
{
    if(text.isEmpty())
        return;

    if(text.toInt() < 121 && text.toInt() > -1)
        if(!_map->getIsObstacle(QPoint(_map->getTargetPoint().x(), text.toInt())))
            _map->setTargetPoint(QPoint(-1, text.toInt(nullptr, 10)));
}

void MainWindow::refreshViewSlot()
{
    QFuture<void> future = QtConcurrent::run([this] {scene->update(ui->mapGraphicsView->sceneRect());});
}






