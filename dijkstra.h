#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "map.h"
#include "ialgorithm.h"

class Dijkstra : public IAlgorithm
{

public:
    explicit Dijkstra(std::shared_ptr<Map> map);

    virtual ~Dijkstra() override;

    void run() override;

    void reset() override;

private:
    std::shared_ptr<Map> _map;
    std::vector<QPoint> _unvisitedNodes;
    std::map<QPoint, double, QPointLessThan> _distances;
    std::vector<QPoint> _visitedCells;
    std::vector<QPoint> _path;
    std::vector<double> _adjacencyMatrix;

    double calculateDistance(const QPoint& point1, const QPoint& point2);
    std::vector<QPoint> getNeighbours(const QPoint& cell);
    void fillAdjacencyMatrix();
    std::vector<QPoint> getPath();
};

#endif // DIJKSTRA_H
