#ifndef POTENTIALFILED_H
#define POTENTIALFILED_H

#include "map.h"
#include "ialgorithm.h"

#include <QTimer>

class PotentialFiled : public IAlgorithm
{

public:

    PotentialFiled(std::shared_ptr<Map> map);

    virtual ~PotentialFiled() override;

    void printPotentialMap();

    void run() override;

    void reset() override;

private:

    std::shared_ptr<Map> _map;
    std::vector<int> _potentialMap;
    void setStartPointPotential();

    std::vector<QPoint> _visitedCells;
    std::vector<QPoint> _path;

    std::vector<QPoint> getNeighbours(const QPoint& cell);
    double calculateDistance(const QPoint& point1, const QPoint& point2);
    double calculateAttractivePotential(const QPoint& position);
    double calculateRepulsivePotential(const QPoint& position);
    double calculatePotential(const QPoint& position);

};

#endif // POTENTIALFILED_H
