#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "renderer.h"
#include "map.h"
#include "algorithmshandler.h"

#include <QMainWindow>
#include <QGraphicsScene>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(std::shared_ptr<Renderer> renderer, std::shared_ptr<Map> map);

    void setUpUI();

public slots:
    void refreshViewSlot();

    void startAlgorithm();

    void reset();

    void changeStatus(const bool& isCalculating);

    void onPathNotFound();

private:
    Ui::MainWindow *ui;
    std::unique_ptr<QGraphicsScene> scene;
    std::shared_ptr<Map> _map;
    std::shared_ptr<Renderer> _renderer;
    std::shared_ptr<AlgorithmsHandler> _algorithmsHandler;
    std::shared_ptr<IAlgorithm> _algorithm;

    bool _isCalculating;

    void onCurrentTextChanged(const QString& text);
    void setStartPointX(const QString& text);
    void setStartPointY(const QString& text);
    void setTargetPointX(const QString& text);
    void setTargetPointY(const QString& text);

    void refresh();

};
#endif // MAINWINDOW_H
