QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    algorithmshandler.cpp \
    astar.cpp \
    bug.cpp \
    dijkstra.cpp \
    dstar.cpp \
    main.cpp \
    mainwindow.cpp \
    map.cpp \
    obstacles.cpp \
    potentialfiled.cpp \
    rapidlyexploringtree.cpp \
    renderer.cpp

HEADERS += \
    algorithmshandler.h \
    ialgorithm.h \
    node.h \
    astar.h \
    bug.h \
    dijkstra.h \
    dstar.h \
    mainwindow.h \
    map.h \
    obstacles.h \
    potentialfiled.h \
    rapidlyexploringtree.h \
    renderer.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
