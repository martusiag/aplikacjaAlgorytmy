#include "dstar.h"
#include <iostream>

DStar::DStar(std::shared_ptr<Map> map)
    : _map(map)
{
    std::cout << "D* instance created!" << std::endl;
}

DStar::~DStar()
{
    std::cout << "D* instance deleted!" << std::endl;
}

void DStar::run()
{

    std::shared_ptr<Node> startNode = std::make_shared<Node>(_map->getStartPoint());
    startNode->gCost = 0;
    startNode->rhsCost = calculateHeuristic(startNode->point, _map->getTargetPoint());
    _openList.push_back(startNode);

    std::shared_ptr<Node> currentNode = startNode;

    while (!_openList.empty())
    {
        if (_openList.front()->rhsCost == INFINITY)
        {
            std::cout << "No path found" << std::endl;
            return;
        }

        if (_openList.front()->rhsCost + _openList.front()->hCost < currentNode->rhsCost + currentNode->hCost)
        {
            currentNode = _openList.front();
            removeNode(_openList, currentNode);
            _closedList.push_back(currentNode);
        }
        else
        {
            double newHCost = calculateHeuristic(_openList.front()->point, _map->getTargetPoint());
            double newRhsCost = INFINITY;
            for (const auto& neighbour : getCellNeighbours(_openList.front()->point))
            {
                if (isInList(_closedList, neighbour))
                {
                    double neighbourRhsCost = neighbour->gCost + calculateHeuristic(neighbour->point, _openList.front()->point);
                    if (neighbourRhsCost < newRhsCost)
                    {
                        newRhsCost = neighbourRhsCost;
                    }
                }
            }
            _openList.front()->hCost = newHCost;
            _openList.front()->rhsCost = newRhsCost;
            addNode(_openList, _openList.front());
            removeNode(_openList, _openList.front());
        }

        if (currentNode->point == _map->getTargetPoint())
        {
            std::vector<QPoint> path;
            std::shared_ptr<Node> current = currentNode;
            while (current != nullptr)
            {
                path.push_back(current->point);
                current = current->parent;
            }
            std::reverse(path.begin(), path.end());
            emit visitedCellsUpdated(path);
            return;
        }

        for (const auto& neighbour : getCellNeighbours(currentNode->point))
        {
            if (isInList(_closedList, neighbour))
                continue;
            double tenativeGScore = currentNode->gCost + calculateHeuristic(currentNode->point, neighbour->point);

            if (!isInList(_openList, neighbour) || tenativeGScore < neighbour->gCost)
            {
                neighbour->gCost = tenativeGScore;
                neighbour->rhsCost = std::min(neighbour->rhsCost, neighbour->hCost + neighbour->gCost);
                neighbour->parent = currentNode;

                if (!isInList(_openList, neighbour))
                {
                    _openList.push_back(neighbour);
                }
                else
                {
                    updateNode(_openList, neighbour);
                }
            }
        }
    }

    std::cout << "No path found" << std::endl;


}

void DStar::reset()
{

}

void DStar::updateNode(std::vector<std::shared_ptr<Node>>& openList, const std::shared_ptr<Node>& node) {
    for (auto it = openList.begin(); it != openList.end(); ++it) {
        if (it->get()->point.x() == node->point.x() && it->get()->point.y() == node->point.y()) {
            if (node->fCost < it->get()->fCost) {
                *it = node;
            }
            return;
        }
    }
}

double DStar::calculateHeuristic(const QPoint &point1, const QPoint &point2)
{
    double dx = point1.x() - point2.x();
    double dy = point1.y() - point2.y();

    return std::sqrt(pow(dx,2) + pow(dy,2));
}

std::vector<std::shared_ptr<Node>> DStar::getCellNeighbours(const QPoint &cell)
{
    std::vector<std::shared_ptr<Node>> neighbors;

    // Check each neighbor position and add to the list if it's within the map bounds
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            if (dx == 0 && dy == 0)
                continue; // skip the current cell

            const QPoint neighbor = cell + QPoint(dx, dy);
            if (neighbor.x() < 0 || neighbor.x() >= _map->getSize().width() ||
                    neighbor.y() < 0 || neighbor.y() >= _map->getSize().height() ||
                    _map->at(neighbor) == NodeState::Obstacle)
                continue; // Skip cells outside the map or obstacles
            neighbors.push_back(std::make_shared<Node>(Node(neighbor)));
        }
    }

    return neighbors;
}

void DStar::removeNode(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node)
{
    for(const auto& entity : list)
    {
        if(entity->point == node->point)
        {
            const auto it = find(list.begin(), list.end(), entity);
            const int index = it - list.begin();
            list.erase(list.begin() + index);
            return;
        }
    }
}

bool DStar::isInList(std::vector<std::shared_ptr<Node> > &list, std::shared_ptr<Node> node)
{
    for(const auto& entity : list)
        if(entity->point == node->point)
            return true;

    return false;
}

void DStar::addNode(std::vector<std::shared_ptr<Node>>& list, std::shared_ptr<Node>& node) {
    // add the front node of _openList to closedList
    list.push_back(node);
}
