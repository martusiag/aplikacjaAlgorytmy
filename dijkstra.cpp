#include "dijkstra.h"
#include <iostream>

constexpr int maxIterations = 100000;

Dijkstra::Dijkstra(std::shared_ptr<Map> map)
    : _map(map)
    , _unvisitedNodes(std::vector<QPoint>(_map->getSize().width() * _map->getSize().height(), QPoint()))
    , _adjacencyMatrix(std::vector<double>(map->getSize().width() * map->getSize().height(), std::numeric_limits<double>::infinity()))
{
    //Fill vector with nodes
    for (int x = 0; x < _map->getSize().width(); ++x) {
        for (int y = 0; y < _map->getSize().height(); ++y) {
            QPoint point(x, y);
            _unvisitedNodes[y * _map->getSize().width() + x] = point;
        }
    }

    for(const QPoint& node : _unvisitedNodes)
    {
        _distances[node] = std::numeric_limits<double>::infinity();
    }
    _distances.at(_map->getStartPoint()) = 0.0;

    std::cout << "Dijkstra instance created!" << std::endl;
}

Dijkstra::~Dijkstra()
{
    reset();
    std::cout << "Dijkstra instance deleted!" << std::endl;
}

void Dijkstra::run()
{
    int iteration = 0;

    fillAdjacencyMatrix();
    while(!_unvisitedNodes.empty())
    {
        if(iteration++ == maxIterations)
        {
            return;
            emit noPathFound();
            emit pathFound(_path);
        }
        // Find the unvisited node with the smallest distance
        QPoint currentCell;
        double minimalDistance = std::numeric_limits<double>::infinity();
        for(const QPoint& node : _unvisitedNodes)
        {
            if(_distances.at(node) < minimalDistance)
            {
                minimalDistance = _distances.at(node);
                currentCell = node;
                _visitedCells.push_back(currentCell);
            }
        }

        _path.push_back(currentCell);

        if(currentCell == _map->getTargetPoint())
        {
            break;
        }

        // Remove the current node from the unvisited set
        auto index = std::find(_unvisitedNodes.begin(), _unvisitedNodes.end(), currentCell);
        _unvisitedNodes.erase(index);

        std::vector<QPoint> neighbours = getNeighbours(currentCell);
        for(const QPoint& neighbour : neighbours)
        {
            //needs to be done this way - we have 17k points
            const int height = _map->getSize().height();
            int i1 = currentCell.y() * _map->getSize().width() + currentCell.x();
            int i2 = neighbour.y() * _map->getSize().width() + neighbour.x();

            if(_adjacencyMatrix[i1 * height + i2] == std::numeric_limits<double>::infinity())
                continue;

            // Calculate the distance from the start node to the neighbor through the current node
            double distanceToNeigbourFromStart = _distances.at(currentCell) + _adjacencyMatrix[i1 * height + i2];

            // Update the distance if it is shorter than the current distance
            if(distanceToNeigbourFromStart < _distances.at(neighbour))
                _distances.at(neighbour) = distanceToNeigbourFromStart;
        }
    }

    getPath();
    emit isCalculating(false);

}

void Dijkstra::reset()
{
    _unvisitedNodes.clear();
    _distances.clear();
    _visitedCells.clear();
    _adjacencyMatrix.clear();
    _path.clear();
}

double Dijkstra::calculateDistance(const QPoint &point1, const QPoint &point2)
{
    double dx = point1.x() - point2.x();
    double dy = point1.y() - point2.y();

    return std::sqrt(pow(dx,2) + pow(dy,2));
}

std::vector<QPoint> Dijkstra::getNeighbours(const QPoint &cell)
{
    std::vector<QPoint> neighbors;

    // Check each neighbor position and add to the list if it's within the map bounds
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            if (dx == 0 && dy == 0)
                continue; // skip the current cell

            const QPoint neighbor = cell + QPoint(dx, dy);
            if (neighbor.x() < 0 || neighbor.x() >= _map->getSize().width() ||
                    neighbor.y() < 0 || neighbor.y() >= _map->getSize().height() ||
                    _map->at(neighbor) == NodeState::Obstacle)
                continue; // Skip cells outside the map or obstacles
            neighbors.push_back(neighbor);
        }
    }

    return neighbors;
}

void Dijkstra::fillAdjacencyMatrix()
{
    for (int x = 0; x < _map->getSize().width(); ++x) {
        for (int y = 0; y < _map->getSize().height(); ++y) {
            QPoint currentCell(x, y);
            // Check if the current cell is reachable
            if (_map->at(currentCell) != NodeState::Obstacle) {
                for (const QPoint& neighbor : getNeighbours(currentCell)) {
                    // Check if the neighbor cell is reachable
                    if (_map->at(neighbor) != NodeState::Obstacle) {
                        // Calculate the distance between the current cell and the neighbor cell
                        double dist = calculateDistance(currentCell, neighbor);
                        // Add the edge to the adjacency matrix

                        int i1 = currentCell.y() * _map->getSize().width() + currentCell.x();
                        int i2 = neighbor.y() * _map->getSize().width() + neighbor.x();
                        _adjacencyMatrix[i1 * _map->getSize().height() + i2] = dist;
                    }
                }
            }
        }
    }
}

std::vector<QPoint> Dijkstra::getPath()
{
    std::vector<QPoint> path;

    // Start at the target cell
    QPoint currentCell = _map->getTargetPoint();

    // Iterate backwards through the visited cells until we reach the start cell
    while(currentCell != _map->getStartPoint())
    {
        // Add the current cell to the path
        path.push_back(currentCell);

        // Find the previous cell in the path
        double minimalDistance = std::numeric_limits<double>::infinity();
        QPoint previousCell;
        for(const QPoint& neighbor : getNeighbours(currentCell))
        {
            if(std::find(_path.begin(), _path.end(), neighbor) != _path.end())
            {
                const int height = _map->getSize().height();
                int i1 = currentCell.y() * _map->getSize().width() + currentCell.x();
                int i2 = neighbor.y() * _map->getSize().width() + neighbor.x();

                if(_adjacencyMatrix[i1 * height + i2] == std::numeric_limits<double>::infinity())
                    continue;

                if(_distances.at(neighbor) < minimalDistance)
                {
                    minimalDistance = _distances.at(neighbor);
                    previousCell = neighbor;
                }
            }
        }

        // Set the current cell to the previous cell
        currentCell = previousCell;
    }

    // Add the start cell to the path
    path.push_back(_map->getStartPoint());

    // Reverse the path so that it goes from start to end
    std::reverse(path.begin(), path.end());

    emit pathFound(path);

    return path;
}

