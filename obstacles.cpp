#include "obstacles.h"

Obstacles::Obstacles(const QSize& mapSize)
    : _mapSize(mapSize)
{

    for (int x = 0; x < _mapSize.width(); ++x)
    {
        topFrame.emplace_back(QPoint(x, 0));         // Top edge
        bottomFrame.emplace_back(QPoint(x, _mapSize.height()-1));        // Bottom edge
    }

    for(int y = 0; y < _mapSize.height(); y++)
    {
        leftFrame.emplace_back(QPoint(0, y));         // Left edge
        rightFrame.emplace_back(QPoint(_mapSize.width()-1, y));        // Right edge
    }

    // Add some additional obstacles
    for (int x = 30; x < 40; ++x)
        for(int y  = 40; y < 50; ++y)
            permamentObstacles.emplace_back(QPoint(x, y));

    for (int y = 60; y < 70; ++y)
        for(int x = 50; x < 55; x++)
        permamentObstacles.emplace_back(QPoint(x, y));

    for (int x = 70; x < 80; ++x)
        for(int y = 60; y < 70; ++y)
            permamentObstacles.emplace_back(QPoint(x, y));

    for (int y = 20; y < 25; ++y)
        for(int x = 30; x < 60; x++)
        permamentObstacles.emplace_back(QPoint(x, y));

    for (int x = 20; x < 25; ++x)
        for(int y = 80; y > 50; y--)
        permamentObstacles.emplace_back(QPoint(x, y));

    for (int y = 40; y < 50; ++y)
        for(int x = 70; x < 75; x++)
        permamentObstacles.emplace_back(QPoint(x, y));


    for(int i = 100; i < 120; i++)
    {
        for(int j = 15; j < 18; j++)
        {
            obstacle1.emplace_back(QPoint(i, j));
        }
    }

    for(int i = 100; i < 103; i++)
    {
        for(int j = 16; j < 35; j++)
        {
            obstacle1.push_back(QPoint(i, j));
        }
    }

    for(int i = 117; i < 120; i++)
    {
        for(int j = 16; j < 35; j++)
        {
            obstacle1.push_back(QPoint(i, j));
        }
    }

    for(int i = 10; i < 20; i++)
    {
        for(int j = 20; j < 30; j++)
        {
            obstacle2.emplace_back(QPoint(i, j));
        }
    }

    for(int i = 112; i < 117; i++)
    {
        for(int j = 90; j > 70; j--)
        {
            obstacle3.emplace_back(QPoint(i, j));
        }
    }

    for(int i = 40; i < 65; i++)
    {
        for(int j = 100; j < 103; j++)
        {
            obstacle4.emplace_back(QPoint(i, j));
        }
    }

    for(int i = 40; i < 43; i++)
    {
        for(int j= 103; j < 113; j++)
        {
            obstacle4.push_back(QPoint(i,j));
        }
    }

    for(int i = 100; i < 112; i++)
    {
        for(int j = 77; j < 82; j++)
        {
            obstacle5.emplace_back(QPoint(i, j));
        }
    }

}

std::vector<QPoint> Obstacles::getObstacles() const
{
    std::vector<QPoint> obstacles;

    obstacles.insert(obstacles.end(), permamentObstacles.begin(), permamentObstacles.end());

    if(_frameObstaclesEnabled)
    {
        obstacles.insert(obstacles.end(), topFrame.begin(), topFrame.end());
        obstacles.insert(obstacles.end(), bottomFrame.begin(), bottomFrame.end());
        obstacles.insert(obstacles.end(), leftFrame.begin(), leftFrame.end());
        obstacles.insert(obstacles.end(), rightFrame.begin(), rightFrame.end());
    }

    if(_firstObstacleEnabled)
        obstacles.insert(obstacles.end(), obstacle1.begin(), obstacle1.end());

    if(_secondObstacleEnabled)
        obstacles.insert(obstacles.end(), obstacle2.begin(), obstacle2.end());

    if(_thirdObstacleEnabled)
        obstacles.insert(obstacles.end(), obstacle3.begin(), obstacle3.end());

    if(_fourthObstacleEnabled)
        obstacles.insert(obstacles.end(), obstacle4.begin(), obstacle4.end());

    if(_fifthObstacleEnabled)
        obstacles.insert(obstacles.end(), obstacle5.begin(), obstacle5.end());

    return obstacles;
}

void Obstacles::updateObstacles() const
{

}

void Obstacles::updateEnabledObstacles(const std::bitset<6>& obstacles)
{
    _frameObstaclesEnabled = obstacles.test(0);
    _firstObstacleEnabled = obstacles.test(1);
    _secondObstacleEnabled = obstacles.test(2);
    _thirdObstacleEnabled = obstacles.test(3);
    _fourthObstacleEnabled = obstacles.test(4);
    _fifthObstacleEnabled = obstacles.test(5);
}
