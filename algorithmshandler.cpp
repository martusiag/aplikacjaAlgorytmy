#include "algorithmshandler.h"
#include <iostream>

AlgorithmsHandler::AlgorithmsHandler(std::shared_ptr<Map> map)
    : _map(map)
{
    std::cout << "AlgorithmsHandler instance created!" << std::endl;
}

AlgorithmsHandler::~AlgorithmsHandler()
{
    std::cout << "AlgorithmsHandler instance deleted!" << std::endl;
}

IAlgorithm* AlgorithmsHandler::createInstance(const QString &algorithm)
{
    if(algorithm.contains("Potential Field"))
        return new PotentialFiled(_map);
    else if(algorithm.contains("Dijkstra"))
        return new Dijkstra(_map);
    else if(algorithm.contains("BUG"))
        return new Bug(_map);
    else if(algorithm.contains("A*"))
        return new AStar(_map);
    else if(algorithm.contains("Rapidly"))
        return new RapidlyExploringTree(_map);

    return nullptr;
}
