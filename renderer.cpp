#include "renderer.h"

#include <QBrush>
#include <QPainter>
#include <iostream>
#include <QTimer>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

constexpr double SQUARE_SIZE = 4.97;

int Renderer::_visitedCellsIterator = -1;

Renderer::Renderer(std::shared_ptr<Map> map)
    : _map(map)
    , _renderer(this)
    , _image(700, 600, QImage::Format_RGB32)
    , _pendingRender(false)
{
    connect(this, &::Renderer::hasNewRenderer, this, [this](const QImage &image){
        _image = image;
        _pendingRender = false;
        emit refreshView();
    });

    drawMap();

}

Renderer::~Renderer()
{

}

const QGraphicsItem *Renderer::rederer() const
{
    return _renderer;
}

QRectF Renderer::boundingRect() const
{
    return QRectF();
}

void Renderer::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->drawImage(0,0, _image);
}

void Renderer::setPendingRenderer(const bool &value)
{
    _pendingRender = value;
}

bool Renderer::getPendingRenderer() const
{
    return _pendingRender;
}

void Renderer::reset()
{
    _visitedCells.clear();
    _path.clear();
    _visitedCellsIterator = -1;
    updateImage();
}

void Renderer::updateVisitedCell(const std::vector<QPoint> &visitedCells)
{
    _visitedCells = visitedCells;
    QTimer::singleShot(1000, this, &Renderer::updateImage);
}

void Renderer::updatePath(const std::vector<QPoint> &path)
{
    _path = path;
    updateImage();
}

void Renderer::updateImage()
{
    QFuture<void> future = QtConcurrent::run([this] {drawMap();});
}

void Renderer::drawMap()
{
    QImage image{_image.size(), _image.format()};
    image.fill(Qt::white);

    QPainter painter(&image);

    if(!painter.isActive())
        return;

    painter.setRenderHint(QPainter::LosslessImageRendering);

    // Draw the grid map
    for (int x = 0; x < _map->getSize().width(); ++x) {
        for (int y = 0; y < _map->getSize().height(); ++y) {
            QPoint pos(x, y);
            int value = _map->at(pos);
            QRectF rect(x * SQUARE_SIZE, y * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
            QColor color;

            // Set the width of the rectangle edges
            QPen pen(QColor(Qt::lightGray), 0.5);  // 2 pixels width
            painter.setPen(pen);

            if (value == NodeState::Free)
                color = QColor(Qt::white);          // white for empty squares
            else if(value == NodeState::Obstacle)
            {
                color = QColor(Qt::black);          // black for occupied squares
            }
            else if(value == NodeState::StartPoint)
                color = QColor(Qt::green);
            else if(value == NodeState::TargetPoint)
                color = QColor(Qt::red);

            painter.setBrush(QBrush(color));
            painter.drawRect(rect);
        }
    }

    if(!_visitedCells.empty())
    {
        // Draw the visited cells
        painter.setBrush(QBrush(QColor(Qt::darkCyan)));
        for (const auto& cell : _visitedCells) {
            QRectF rect(cell.x() * SQUARE_SIZE, cell.y() * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
            painter.drawRect(rect);
        }
    }

    // Draw path
    if(!_path.empty())
    {
        painter.setBrush(QBrush(QColor(Qt::cyan)));
        for (const auto& cell : _path) {
            QRectF rect(cell.x() * SQUARE_SIZE, cell.y() * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
            painter.drawRect(rect);
        }
    }

    emit hasNewRenderer(image);

}
