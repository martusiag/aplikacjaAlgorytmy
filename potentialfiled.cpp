#include "potentialfiled.h"
#include <math.h>
#include <iostream>

constexpr int MaxPotentialValue = 2147483647;
constexpr int RepulsiveForceFactor = 50;
constexpr int AttractiveForceFactor = 100;
constexpr double MinimumDistanceToObstacle = 1.0;

constexpr int maxIterations = 100000;

PotentialFiled::PotentialFiled(std::shared_ptr<Map> map)
    : _map(map)
{
    std::cout << "Potential Field instance created!" << std::endl;
}

PotentialFiled::~PotentialFiled()
{
    reset();
    std::cout << "Potential Field instance deleted!" << std::endl;
}

void PotentialFiled::setStartPointPotential()
{
    QPoint startPoint = _map->getStartPoint();
    if (startPoint.x() < 0 || startPoint.x() >= _map->getSize().width() ||
            startPoint.y() < 0 || startPoint.y() >= _map->getSize().height()) {
        // Start point is outside the bounds of the map, so do nothing
        std::cout << "START POINT IS OUT OF RANGE " << startPoint.x() << " " << startPoint.y()  << " " << _map->getSize().width() << "x" << _map->getSize().height()  << std::endl;
        return;
    }
    int index = startPoint.y() * _map->getSize().width() + startPoint.x();
    //std::cout << "Setting Start point potential to 0 for coordinate: "  << index << std::endl;
    _potentialMap.at(index) = 0;
    //std::cout << _potentialMap.at(index) << std::endl;
}

void PotentialFiled::printPotentialMap()
{
    int col = 0;
    for(const int& potential : _potentialMap)
    {
        std::cout << potential << " ";
        if(col++ == 10)
         {
            std::cout  << "\n";
            col = 0;
        }
    }

     for(const int& point : _potentialMap)
     {
        if(point == 0)
            std::cout << "POINT  0 IS START " << &point - &_potentialMap[0] << std::endl;
     }
}

void PotentialFiled::run()
{
    for(int i = 0; i < _map->getSize().width() * _map->getSize().height(); i++)
        _potentialMap.push_back(MaxPotentialValue);

    for(int i = 0; i < _map->getSize().width(); i++)
    {
        for(int j = 0; j < _map->getSize().height(); j++)
        {
            if(_map->at(QPoint(i,j)) == NodeState::StartPoint)
                continue;
            _potentialMap.at(j * _map->getSize().width() + i) = calculatePotential(QPoint(i,j));
        }
    }

    setStartPointPotential();

    QPoint currentCell = _map->getStartPoint();

    _visitedCells.push_back(currentCell);
    _path.push_back(currentCell);

    int iteration = 0;

    while(currentCell != _map->getTargetPoint())
    {
        if(iteration++ == maxIterations)
        {
            return;
            emit noPathFound();
            emit pathFound(_path);
        }

        std::vector<QPoint> neighbours = getNeighbours(currentCell);
        int lowestPotential = MaxPotentialValue;
        QPoint nextCell(-1,-1);

        for(const QPoint& neighbor : neighbours)
        {
            if (!std::any_of(_visitedCells.begin(), _visitedCells.end(),
                    [&](const QPoint& visited){ return visited == neighbor; }))
            {
                const int index = neighbor.y() * _map->getSize().width() + neighbor.x();
                const int neighborPotential = _potentialMap.at(index);

                if(neighborPotential < lowestPotential)
                {
                    lowestPotential = neighborPotential;
                    nextCell = neighbor;
                    _visitedCells.push_back(nextCell);
                    emit visitedCellsUpdated(_visitedCells);
                }
            }
        }

        if(nextCell != QPoint(-1,-1))
        {
            currentCell = nextCell;
            _path.push_back(currentCell);
        }
        else
        {
            _path.pop_back();
        }
    }

    _path.pop_back();

    emit pathFound(_path);
    emit isCalculating(false);
}

void PotentialFiled::reset()
{
    _potentialMap.clear();
    _visitedCells.clear();
    _path.clear();
}

std::vector<QPoint> PotentialFiled::getNeighbours(const QPoint &cell)
{
    std::vector<QPoint> neighbors;

    // Check each neighbor position and add to the list if it's within the map bounds
    for (int dx = -1; dx <= 1; dx++)
    {
        for (int dy = -1; dy <= 1; dy++)
        {
            if (dx == 0 && dy == 0)
                continue; // skip the current cell

            const QPoint neighbor = cell + QPoint(dx, dy);
            if (neighbor.x() < 0 || neighbor.x() >= _map->getSize().width() ||
                    neighbor.y() < 0 || neighbor.y() >= _map->getSize().height() ||
                    _map->at(neighbor) == NodeState::Obstacle)
                    continue; // Skip cells outside the map or obstacles
            neighbors.push_back(neighbor);
        }
    }

    return neighbors;
}

double PotentialFiled::calculateDistance(const QPoint &point1, const QPoint &point2)
{
    double dx = point1.x() - point2.x();
    double dy = point1.y() - point2.y();

    return std::sqrt(pow(dx,2) + pow(dy,2));
}

double PotentialFiled::calculateAttractivePotential(const QPoint &position)
{
    double distance = calculateDistance(position, _map->getTargetPoint());

    return 0.5 * AttractiveForceFactor * pow(distance,2);
}

double PotentialFiled::calculateRepulsivePotential(const QPoint &position)
{
    double repulsivePotential = 0;
    for(const QPoint& obstacle : _map->getObstacles())
    {
        double distance = calculateDistance(position, obstacle);
        if(distance < MinimumDistanceToObstacle)
            repulsivePotential += 0.5 * RepulsiveForceFactor * pow(1 / distance - 1 / MinimumDistanceToObstacle,2);
    }

    return repulsivePotential;
}

double PotentialFiled::calculatePotential(const QPoint &position)
{
    return calculateAttractivePotential(position) + calculateRepulsivePotential(position);
}

