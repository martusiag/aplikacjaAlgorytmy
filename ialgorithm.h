#ifndef IALGORITHM_H
#define IALGORITHM_H

#include <QPoint>
#include <QObject>

class IAlgorithm : public QObject
{
    Q_OBJECT
public:

    virtual ~IAlgorithm() {};

    virtual void run() = 0;

    virtual void reset() = 0;

signals:

    void visitedCellsUpdated(const std::vector<QPoint>& visitedCells);

    void pathFound(const std::vector<QPoint>& path);

    void isCalculating(const bool&);

    void noPathFound();
};


#endif // IALGORITHM_H
