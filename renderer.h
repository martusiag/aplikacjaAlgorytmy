#ifndef RENDERER_H
#define RENDERER_H

#include "map.h"

#include <QObject>
#include <QPaintEvent>
#include <QGraphicsItem>

class Renderer : public QObject, public QGraphicsItem
{
   Q_OBJECT

public:
    Renderer(std::shared_ptr<Map> map);
    ~Renderer();;
    const QGraphicsItem* rederer() const;

    QRectF boundingRect() const override;

    void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget *widget) override;

    void drawVisitedCells(const std::vector<QPoint> &visitedCells);

    void setPendingRenderer(const bool& value);

    bool getPendingRenderer() const;

    void reset();

signals:

    void refreshView();

    void hasNewRenderer(const QImage&);

public slots:
    void updateVisitedCell(const std::vector<QPoint>& visitedCells);

    void updatePath(const std::vector<QPoint>& path);

    void updateImage();

private:
    std::shared_ptr<Map> _map;

    QGraphicsItem* _renderer;

    std::vector<QPoint> _visitedCells;

    std::vector<QPoint> _path;

    static int _visitedCellsIterator;

    QImage _image;

    bool _pendingRender;

    void drawMap();


};

#endif // RENDERER_H
