#ifndef RAPIDLYEXPLORINGTREE_H
#define RAPIDLYEXPLORINGTREE_H

#include "map.h"
#include "node.h"
#include "ialgorithm.h"

class Tree {
public:
    // Add a node to the tree
    void addNode(Node node) {
        nodes.push_back(node);
    }

    // Add an edge between two nodes
    void addEdge(Node node1, Node node2) {
        // Find the indices of the nodes in the nodes vector
        int index1 = -1, index2 = -1;
        for (ulong i = 0; i < nodes.size(); i++) {
            if (nodes[i].point == node1.point) {
                index1 = i;
            }
            if (nodes[i].point == node2.point) {
                index2 = i;
            }
        }
        // Add the edge if both nodes are in the tree
        if (index1 != -1 && index2 != -1) {
            edges.push_back(std::make_pair(index1, index2));
        }
    }

    Tree(const QPoint& point) { nodes.push_back(Node(point)); }

    std::vector<Node> nodes; // Vector to store the nodes
    std::vector<std::pair<int, int>> edges; // Vector to store the edges as pairs of node indices
};


class RapidlyExploringTree : public IAlgorithm
{

public:
    RapidlyExploringTree(std::shared_ptr<Map> map);

    virtual ~RapidlyExploringTree() override;

    void run() override;

    void reset() override;

private:

    std::shared_ptr<Map> _map;

    std::vector<QPoint> _visitedCells;
    std::vector<QPoint> _path;

    Node randomConfig();
    Node* nearest(Tree& tree, Node q);
    Node newConfig(Node q, Node q_near, double delta_q);

    bool isColisionFree(const Node& node1, const Node& node2);
    std::vector<Node> getCellsOnLine(const Node &node1, const Node &node2);

};

#endif // RAPIDLYEXPLORINGTREE_H
