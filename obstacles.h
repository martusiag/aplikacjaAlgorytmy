#ifndef OBSTACLES_H
#define OBSTACLES_H

#include <vector>
#include <QPoint>
#include <QSize>

#include <bitset>

class Obstacles
{
public:

    Obstacles(const QSize& mapSize);

    std::vector<QPoint> getObstacles() const;

    void updateObstacles() const;

    void updateEnabledObstacles(const std::bitset<6>& obstacles);

private:

    QSize _mapSize;

    bool _frameObstaclesEnabled;

    std::vector<QPoint> topFrame;

    std::vector<QPoint> bottomFrame;

    std::vector<QPoint> leftFrame;

    std::vector<QPoint> rightFrame;

    std::vector<QPoint> permamentObstacles;

    std::vector<QPoint> obstacle1;

    std::vector<QPoint> obstacle2;

    std::vector<QPoint> obstacle3;

    std::vector<QPoint> obstacle4;

    std::vector<QPoint> obstacle5;

    bool _firstObstacleEnabled;
    bool _secondObstacleEnabled;
    bool _thirdObstacleEnabled;
    bool _fourthObstacleEnabled;
    bool _fifthObstacleEnabled;



};

#endif // OBSTACLES_H
